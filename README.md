# Instructions

1-Install [Node.Js](https://nodejs.org/en/)

- To check if you have Node.js installed, run this command in your terminal:
  ```
      node -v
  ```
- To confirm that you have npm installed you can run this command in your terminal:
  ```
      node -v
  ```

2- Clone the project to a directory of your choice  
3-Navigate to that directory  
4-From cmd line execute "npm install"  
5-In the cmd line execute the program by typing "npm run CalculateOrder" followed by url of the csv document and the parameters that you wich.

Example:

```
    npm run CalculateOrder Catalog.txt P4 6 P10 5 P12 1
```

Output will be :

```
    Total:  � 4,151.25
```

If some product does not exist on the list or the quantity is less than 1 it will return:

```sh

Error Code 1 : The product P12 is out of stock

```