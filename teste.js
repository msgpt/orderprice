const formatter = new Intl.NumberFormat("pt-PT", {
  style: "currency",
  currency: "EUR"
});

const argumentos = process.argv.slice(2);

function CalculateOrder(path = argumentos[1], args = argumentos) {
  let fs = require("fs");
  let fastcsv = require("fast-csv");

  let readableStreamInput = fs.createReadStream(path);
  let csvData = [];

  fastcsv
    .fromStream(readableStreamInput, {
      headers: ["produto", "quantidade", "preco"]
    })
    .on("data", data => {
      let rowData = {};

      Object.keys(data).forEach(current_key => {
        rowData[current_key] = data[current_key];
      });

      csvData.push(rowData);
    })
    .on("end", () => {
      let total = 0;
      const regex = /^p/i;
      args.forEach((val, index) => {
        if (regex.test(val)) {
          const arrProduct = csvData.filter(
            item => item.produto === val.toUpperCase()
          );
          if (
            typeof arrProduct[0] !== "undefined" &&
            parseFloat(arrProduct[0].quantidade) > 0
          ) {
            total += 1.23 * (parseFloat(arrProduct[0].preco) * args[index + 1]);
          } else {
            console.log(`Error Code 1 : The product ${val} is out of stock`);
          }
        }
      });
      console.log("Total: ", formatter.format(total));
    });
}

module.exports.CalculateOrder = CalculateOrder();
